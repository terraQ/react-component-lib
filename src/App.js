import Accordion from './Components/Accordion/Accordion.tsx';
import './App.css';

function App() {
  return <Accordion expanded={true} />;
}

export default App;
