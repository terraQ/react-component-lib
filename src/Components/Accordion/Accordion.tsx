import React, { useState } from 'react';
import styles from './Accordion.module.scss';

export interface AccordionProps {
  readonly expanded: boolean;
};

const Accordion = ({ expanded }: AccordionProps) => {
  const [isExpanded, setIsExpanded] = useState<boolean>(expanded);

  const handleOnClick = () => setIsExpanded(!isExpanded);

  return (
    <div id="accordionGroup" className={styles.accordion}>
      <h3>
        <button
          aria-controls='sect1'
          aria-expanded={isExpanded}
          className={styles["accordion-trigger"]}
          id='accordion1id'
          type='button'
          onClick={handleOnClick}
        >
          <span className={styles["accordion-title"]}>
            Lorem Ipsum
            <span className={styles["accordion-icon"]}></span>
          </span>
        </button>
      </h3>
      {isExpanded && <div
        aria-labelledby='accordion1id'
        className={styles["accordion-panel"]}
        id='sect1'
        role='region'
      >
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed ornare metus, eu rutrum ex. Integer felis erat, pretium eu commodo eget, blandit sed ligula. Nulla eu arcu non elit scelerisque egestas. Etiam et augue nec dolor facilisis iaculis. Suspendisse dignissim laoreet hendrerit. Maecenas non lectus nibh. Nullam et augue quam. Phasellus lobortis venenatis tempor. Nullam interdum erat eu fringilla dignissim.</p>
      </div>}
    </div>
  );
};

export default Accordion;
